import * as Actions from '../actions';
const initialState = {
  name: '',
  data:[],
  loading:false
}

const form = (state = initialState, action) => {
  switch (action.type) {

    case Actions.SET_NAME: {
      return {
        ...state,
        name: action.payload,
      };
    }

    case Actions.SET_DATA: {
      return {
        ...state,
        data: action.payload,
      };
    }

    case Actions.SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      };
    }

    default: return state;
  }
};

export default form;

import React from 'react';
import ReactDOM from 'react-dom';
import {AutoSizer, List} from 'react-virtualized';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import 'react-virtualized/styles.css';
import * as Actions from '../../actions'

import data from '../../config/city.list.min.json';
import './Form.css';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      listData: [],
    };
  }

  componentDidMount() {
    this.setState({listData: data})

  }

  onClickCity = (city) => {
    if (!this.props.loading) {
      this.props.loadCity(city.id);
    }
  }

  renderRow = ({
    key,
    index,
    isScrolling,
    isVisible,
    style
  }) => {
    let city = this.state.listData[index];
    return (
      <div className="wm-list-item" title={`${city.name} - ${city.country}`} onClick={() => this.onClickCity(city)} key={key} style={style}>
        {`${city.name} - ${city.country}`}
      </div>
    )
  }
  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  }

  submit = ()=>{
    this.props.loadCityByName(this.state.name);
  }

  render() {
    let {listData} = this.state;
    return (
      <div className="wm-form">
        <div className="wm-form-search">
          <h1>Type city</h1>
          <div>
            <input
              type="text"
              name="name"
              placeholder="Enter a city"
              value={this.state.name}
              onChange={this.handleChange}
            />
            <button onClick={this.submit}>Find</button>
          </div>
        </div>
        <div>OR</div>
        <h1>Select city</h1>
        <div className="wm-form-list">
          <AutoSizer>
            {({height, width}) => (
              <List
                width={width}
                height={height}
                rowCount={listData.length}
                rowHeight={30}
                rowRenderer={this.renderRow}
              />
            )}
          </AutoSizer>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    loadCity: Actions.loadCity,
    loadCityByName: Actions.loadCityByName,
    setDisplayName: Actions.setDisplayName,
    setData: Actions.setData,
    setLoading: Actions.setLoading
  }, dispatch);
}

function mapStateToProps({form}) {
  return {
    name: form.name,
    loading: form.loading
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form);

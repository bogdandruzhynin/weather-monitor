import * as API from '../api';
import * as util from '../util';

export const SET_NAME = 'SET_NAME';
export const SET_DATA = 'SET_DATA';
export const SET_LOADING = 'SET_LOADING';
export const SET_ERROR = 'SET_LOADING';

export const loadCity = id => {
  return (dispatch, getState) => {
    dispatch(setLoading(true));
    API.getForecast(id)
    .then(res => {

      dispatch(setData(util.formatData(res.data.list)));
      dispatch(setDisplayName(res.data.city.name));
      dispatch(setLoading(false));

    }).catch(errorHandler)
    dispatch({
      type: SET_NAME,
      payload: name,
    })
  }
}

export const loadCityByName = name => {
  return (dispatch, getState)=>{
    dispatch(setLoading(true));
    API.getForecastByName(name)
    .then(res => {

      dispatch(setData(util.formatData(res.data.list)));
      dispatch(setDisplayName(res.data.city.name));
      dispatch(setLoading(false));

    }).catch(errorHandler)
    dispatch({
      type: SET_NAME,
      payload: name,
    })
  }
}

export const setDisplayName = name => {
  return (dispatch, getState) => {
    dispatch({
      type: SET_NAME,
      payload: name,
    })
  }
}

export const setData = data => {
  return (dispatch, getState) => {
    console.log('update!')
    dispatch({
      type: SET_DATA,
      payload: data,
    })
  }
}

export const setLoading = state => {
  return (dispatch, getState) => {
    console.log('update!');!
    dispatch({
      type: SET_LOADING,
      payload: state,
    })
  }
}

const errorHandler = err => {
  return (dispatch, getState) => {
    setLoading(false);
    dispatch({
      type: SET_ERROR,
      payload: err,
    })
  }
}
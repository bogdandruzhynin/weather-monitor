import React from 'react';

import Form from './components/Form';
import Graph from './components/Graph';

import './App.css'

const App = () => (
  <div className="wm-wrapper">
    <Form />
    <Graph />
  </div>
);

export default App;
